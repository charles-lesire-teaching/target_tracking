#include <chrono>
#include <functional>

#include "rclcpp/rclcpp.hpp"
#include "geometry_msgs/msg/twist.hpp"
#include "geometry_msgs/msg/point.hpp"
#include "sensor_msgs/msg/camera_info.hpp"

using namespace std::chrono_literals;

class ControlNode : public rclcpp::Node
{
  public:
    ControlNode()
    : Node("control_node")
    {
      publisher_ = this->create_publisher<geometry_msgs::msg::Twist>("cmd_vel", 1);
      point_sub_ = this->create_subscription<geometry_msgs::msg::Point>("point", 1, 
        [=](const geometry_msgs::msg::Point& msg) { this->detection_callback(msg); });
    }

  private:
    void detection_callback(const geometry_msgs::msg::Point& point)
    {
      auto cmd = geometry_msgs::msg::Twist();
      cmd.angular.z = 0.5;
      publisher_->publish(cmd);
    }
    rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr publisher_;
    rclcpp::Subscription<geometry_msgs::msg::Point>::SharedPtr point_sub_;
};

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<ControlNode>());
  rclcpp::shutdown();
  return 0;
}
